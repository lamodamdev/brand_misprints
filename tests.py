# coding=utf-8

from unittest import TestCase
from word_translator import WordTranslator


class TestWordTranslator(TestCase):

    def setUp(self):
        self.w = WordTranslator()

    def test_process_string(self):
        for dictionary in self.w.translate_dictionaries:
            for key in dictionary:
                self.assertEqual(self.w.translate_string(key, dictionary), dictionary[key])
        for dictionary in self.w.layout_dictionaries:
            for key in dictionary:
                self.assertEqual(self.w.change_layout_of_string(key, dictionary), dictionary[key])

    def test_process_word(self):
        self.assertEqual(
            sorted(self.w.process_word('puma')),
            ['gevf', 'puma', u'згьф', u'пума']
        )
