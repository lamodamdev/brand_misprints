import csv
from sys import argv


def get_filtered_result(source_path, result_path, filter_coefficient):
    with open(source_path, 'rb') as source_file:
        reader = csv.reader(source_file, delimiter=',', quotechar='|')
        with open(result_path, 'wb') as filtered_file:
            writer = csv.writer(filtered_file, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for row in reader:
                if float(row[-1]) <= filter_coefficient:
                    writer.writerow(row)

if __name__ == "__main__":
    filter_coefficient = 0.25
    if len(argv) == 4:
        filter_coefficient = float(argv.pop())
    script_name, full_result_file_path, filtered_result_file_path = argv
    get_filtered_result(full_result_file_path, filtered_result_file_path, filter_coefficient)
