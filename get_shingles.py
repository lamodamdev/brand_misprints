from sys import argv
import csv


def process_raw_request(request):
    return request.decode('utf-8').strip().lower()


def get_users_requests(file_path):
    with open(file_path, 'rb') as request_file:
        for row in request_file.readlines():
            yield process_raw_request(row)


def get_shingles_for_request(request):
    words = request.split()
    n = len(words)
    for shingle_len in xrange(1, n + 1):
        for j in xrange(n - shingle_len + 1):
            yield ' '.join(words[j:j+shingle_len])


def get_all_shingles(file_path):
    result = {}
    print 'Shingles getting is started'
    for request in get_users_requests(file_path):
        for shingle in get_shingles_for_request(request):
            result[shingle] = request
    print 'Shingles getting is finished'
    return result


def write_shingles_to_file(shingles, file_path):
    with open(file_path, 'wb') as shingles_file:
        writer = csv.writer(shingles_file, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for shingle in shingles:
            writer.writerow([shingle.encode('utf-8')])


if __name__ == "__main__":
    script_name, shingles_source_file_path, shingles_result_file_path = argv
    shingles = get_all_shingles(shingles_source_file_path)
    print 'Shingles:', len(shingles)
    write_shingles_to_file(shingles, shingles_result_file_path)
