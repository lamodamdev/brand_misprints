import csv
from sys import argv
from levenshtein import get_levenshtein_distance


def get_shingles(file_path):
    with open(file_path, 'rb') as shingles_file:
        reader = csv.reader(shingles_file, delimiter=',', quotechar='|')
        return [row[0].decode('utf-8') for row in reader]


def get_brands(file_path):
    with open(file_path, 'rb') as brands_file:
        reader = csv.reader(brands_file, delimiter=',', quotechar='|')
        return {row[0].decode('utf-8'): row[1].decode('utf-8') for row in reader}


def find_all_distances(shingle, brands):
    return {brand: get_levenshtein_distance(shingle, brand) for brand in brands}


def get_shortest_distances(shingles_source, brands_source, result_file_path):
    with open(result_file_path, 'wb') as result_file:
        writer = csv.writer(result_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        i = 0
        shingles = get_shingles(shingles_source)
        print len(shingles)
        brands = get_brands(brands_source)
        print len(brands)
        for shingle in shingles:
            all_distances = find_all_distances(shingle, brands)
            shortest_distance = min(all_distances.values())
            aliases_with_shortest_distance = [key for key in all_distances if all_distances[key] == shortest_distance]
            for alias in aliases_with_shortest_distance:
                writer.writerow([
                    shingle.encode('utf-8'),
                    shortest_distance,
                    alias.encode('utf-8'),
                    brands[alias].encode('utf-8'),
                    float(shortest_distance)/float(len(shingle))
                ])
            i += 1
            if i % 1000 == 0:
                print "{} shingles are processed".format(i)

if __name__ == "__main__":
    script_name, shingles_source, brands_source, result_file_path = argv
    get_shortest_distances(shingles_source, brands_source, result_file_path)
