# coding=utf-8

COMMON_SYMBOLS = [
    ' ',
    '.',
    ',',
    '"',
    "'",
    '&',
    '-'
]

COMMON_SYMBOLS_DICTIONARY = {i: [i] for i in COMMON_SYMBOLS}

TRANSLITERATION_RU_EN = {
    u"б": ["b"],
    u"c": ["c"],
    u"к": ["c", "k"],
    u"ч": ["ch"],
    u"д": ["d"],
    u"е": ["e"],
    u"э": ["e"],
    u"ф": ["f", "ph"],
    u"г": ["g"],
    u"х": ["h"],
    u"дж": ["j"],
    u"л": ["l"],
    u"м": ["m"],
    u"н": ["n"],
    u"п": ["p"],
    u"с": ["s", "th"],
    u"ш": ["sh"],
    u"щ": ["shch"],
    u"т": ["t"],
    u"ц": ["ts"],
    u"у": ["u"],
    u"в": ["v", "w"],
    u"кс": ["x"],
    u"гз": ["x"],
    u"й": ["y"],
    u"ы": ["y"],
    u"я": ["ya"],
    u"ё": ["yo"],
    u"ю": ["yu"],
    u"з": ["z", "th"],
    u"ж": ["zh"],
    u"и": ["i", "e"],
    u"о": ["o"],
    u"а": ["a"],
    u"р": ["r"],
    u"ь": ["'", ""],
    u"ъ": [""]
}

TRANSLITERATION_EN_RU = {
    "a": [u"а"],
    "b": [u"б"],
    "c": [u"c", u"к", u"ч"],
    "ch": [u"ч"],
    "d": [u"д"],
    "e": [u"и", u"е", u"э"],
    "f": [u"ф"],
    "g": [u"г"],
    "h": [u"х"],
    "i": [u"и"],
    "k": [u"к"],
    "j": [u"дж"],
    "kh": [u"х", u"к"],
    "l": [u"л"],
    "m": [u"м"],
    "n": [u"н"],
    "o": [u"о"],
    "p": [u"п"],
    "r": [u"р"],
    "s": [u"с"],
    "sh": [u"ш"],
    "shch": [u"щ"],
    "t": [u"т"],
    "ts": [u"ц"],
    "u": [u"у"],
    "v": [u"в"],
    "w": [u"в"],
    "x": [u"кс", u"гз"],
    "y": [u"ай", u"й", u"ы"],
    "ya": [u"я"],
    "yo": [u"ё"],
    "yu": [u"ю"],
    "z": [u"з"],
    "zh": [u"ж"],
    "ee": [u"и"],
    "ea": [u"и"],
    "oa": [u"о"],
    "oe": [u"о"],
    "ai": [u"а"],
    "ay": [u"а"],
    "ui": [u"у"],
    "ie": [u"ай"],
    "ye": [u"и"],
    "tch": [u"ч"],
    "exh": [u"игз"],
    "rh": [u"р"],
    "ph": [u"ф"],
    "wh": [u"ув", u"ху"],
    "th": [u"с", u"з", u"т"],
    "gh": [u"г"],
    "mb": [u"м"],
    "mt": [u"т"],
    "gn": [u"н"]
    }

KEYBOARD_LAYOUT_RU_EN = {
    u"а": "f",
    u"б": ",",
    u"в": "d",
    u"г": "u",
    u"д": "l",
    u"е": "t",
    u"ё": "`",
    u"ж": ";",
    u"з": "p",
    u"и": "b",
    u"й": "q",
    u"к": "r",
    u"л": "k",
    u"м": "v",
    u"н": "y",
    u"о": "j",
    u"п": "g",
    u"р": "h",
    u"с": "c",
    u"т": "n",
    u"у": "e",
    u"ф": "a",
    u"х": "[",
    u"ц": "w",
    u"ч": "x",
    u"ш": "i",
    u"щ": "o",
    u"ъ": "]",
    u"ы": "s",
    u"ь": "m",
    u"э": "'",
    u"ю": ".",
    u"я": "z"
}

KEYBOARD_LAYOUT_EN_RU = {KEYBOARD_LAYOUT_RU_EN[key]: key for key in KEYBOARD_LAYOUT_RU_EN}
TRANSLITERATION_RU_EN.update(COMMON_SYMBOLS_DICTIONARY)
TRANSLITERATION_EN_RU.update(COMMON_SYMBOLS_DICTIONARY)


class WordTranslator(object):

    translate_dictionaries = [
        TRANSLITERATION_RU_EN,
        TRANSLITERATION_EN_RU
    ]
    layout_dictionaries = [
        KEYBOARD_LAYOUT_RU_EN,
        KEYBOARD_LAYOUT_EN_RU
    ]

    def translate_string(self, string, dictionary):
        strings = [string]
        keys = dictionary.keys()
        for i in xrange(4, 0, -1):
            keys_with_this_len = [key for key in keys if len(key) == i and key in strings[0]]
            for key in keys_with_this_len:
                strings = [s.replace(key, value)
                           for s in strings
                           for value in dictionary[key]]
        return strings

    def change_layout_of_string(self, string, dictionary):
        return ''.join([dictionary.get(letter, letter) for letter in string])

    def process_word(self, word):
        results = []
        results.extend([i for dictionary in self.translate_dictionaries for i in self.translate_string(word, dictionary)])
        results.extend([self.change_layout_of_string(string, dictionary)
                        for dictionary in self.layout_dictionaries
                        for string in results])
        return list(set([result for result in results if result]))
