import csv
from sys import argv


def group_by_brands(info):
    result = {}
    for entry in info:
        if entry['brand'] not in result:
            result[entry['brand']] = []
        result[entry['brand']].append(entry)
    return result


def get_same_brands(result):
    return [result[i] for i in result if len(result[i]) > 1]


def get_different_brands(result):
    return [result[i] for i in result if len(result[i]) == 1]


def get_filtered_result(source_path, same_brands_path, different_brands_path, other_result_path):
    with open(source_path, 'rb') as source_file:
        reader = csv.reader(source_file, delimiter=',', quotechar='"')
        with open(same_brands_path, 'wb') as same_brands_file, \
            open(different_brands_path, 'wb') as different_brands_file, \
            open(other_result_path, 'wb') as other_result_file:
            writer_same = csv.writer(same_brands_file, delimiter=';',
                                     quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_different = csv.writer(different_brands_file, delimiter=';',
                                          quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_other = csv.writer(other_result_file, delimiter=';',
                                      quotechar='"', quoting=csv.QUOTE_MINIMAL)
            result = {}
            for row in reader:
                if row[0] not in result:
                    result[row[0]] = []
                result[row[0]].append({'misprint': row[2], 'brand': row[3], 'distance': row[1], 'coefficient': row[4]})
            doubles = {i: result[i] for i in result if len(result[i]) > 1}
            other = {i: result[i] for i in result if len(result[i]) == 1}
            for shingle in other:
                row = other[shingle][0]
                writer_other.writerow([shingle, row['distance'], row['misprint'], row['brand'], row['coefficient']])
            for shingle in doubles:
                group_by = group_by_brands(doubles[shingle])
                for brand in get_same_brands(group_by):
                    for row in brand:
                        writer_same.writerow([shingle, row['distance'], row['misprint'], row['brand'], row['coefficient']])
                for brand in get_different_brands(group_by):
                    for row in brand:
                     writer_different.writerow([shingle, row['distance'], row['misprint'], row['brand'], row['coefficient']])

if __name__ == "__main__":
    script_name, source_file_path, same_brands_path, different_brands_path, other_result_path = argv
    get_filtered_result(source_file_path, same_brands_path, different_brands_path, other_result_path)
