from sys import argv
import csv
from word_translator import WordTranslator


def get_brands(file_path):
    with open(file_path, 'rb') as brand_file:
        reader = csv.reader(brand_file, delimiter=';', quotechar='|')
        return {row[1].decode('utf-8'): row[0].decode('utf-8') for row in reader}


def process_brands(brands, word_processor):
    print "User's brands:", len(brands)
    unique_correct_brands = list(set(brands.values()))
    print "Unique correct brands:", len(unique_correct_brands)
    transliterations = {}
    for brand in unique_correct_brands:
        for word in word_processor.process_word(brand):
            transliterations[word] = brand
    brands.update(transliterations)
    print "Transliterations and keyboard layout corrections:", len(transliterations)
    return brands


def write_brands_to_file(brands, file_path):
    with open(file_path, 'wb') as brands_file:
        writer = csv.writer(brands_file, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for brand in brands:
            writer.writerow([brand.encode('utf-8'), brands[brand].encode('utf-8')])


if __name__ == "__main__":
    script_name, brands_source_file_path, brands_result_file_path = argv
    raw_brands = get_brands(brands_source_file_path)
    word_processor = WordTranslator()
    brands = process_brands(raw_brands,word_processor)
    print 'Brands:', len(brands)
    write_brands_to_file(brands, brands_result_file_path)
