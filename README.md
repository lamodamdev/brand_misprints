# Commands:

> python get_brands.py #SOURCE_PATH# #RESULT_PATH#	
> python get_shingles.py #SOURCE_PATH# #RESULT_PATH#	
> python get_shortest_distances.py #SHINGLES_PATH# #BRANDS_PATH# #RESULT_PATH#	
> python get_filtered_distances.py #DISTANCES_SOURCE# #RESULT_PATH# #(optional)FILTER_COEFFICIENT#
> python get_double_shingles.py #FILTERED_DISTANCES_PATH# #SAME_BRANDS_PATH# #DIFFERENT_BRANDS_PATH# #WITHOUT_DOUBLES_PATH#

# Tests:

> $ nosetests tests.py
